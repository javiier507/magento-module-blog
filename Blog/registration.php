<?php
/**
 * Created by PhpStorm.
 * User: carlospenalba
 * Date: 2/23/19
 * Time: 11:50 AM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Toptal_Blog',
    __DIR__
);